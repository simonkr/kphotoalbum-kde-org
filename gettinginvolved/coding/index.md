---
layout: page
title: First Step as a KPhotoAlbum Developer
---

## First Step as a KPhotoAlbum Developer

Once you have decided to participate in the development of KPhotoAlbum, you need to get your hands on the sources. See details <a href="/download/git/">on how to check out kphotoalbum from git</a>.

When you have made a change, please mail a patches to the <a href="/communication/#communication_mailing-list">mailing list</a> (see details below).

Please read this page and the <a href="https://community.kde.org/KPhotoAlbum/build_instructions">build instructions</a> to learn how to compile the sources.

## Creating a patch

Imagine you have your changes committed in <tt>kphotoalbum-my-commit</tt> and the version prior the changes in <tt>kphotoalbum-prev-revision</tt>, then run this command to produce a patch:

<tt>git format-patch kphotoalbum-prev-revision kphotoalbum-my-commit</tt>

You can mail the patch that is produced by this command to the KPhotoAlbum mailing list.

## Getting git write access

Once you have supplied a number of patches, it is time to get your own git account. While sending patches is a great way for you to align up with coding standards and ways of doing things in KPhotoAlbum, it is not effective on a larger scale.

Read <a href="http://community.kde.org/Sysadmin/GitKdeOrgManual#How_to_get_read-write_developer_access">How to get read-write developer access</a>.
