---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
sorted: 5
---

# Get Involved!

Once long time ago KPhotoAlbum was a one man project, but fortunately it isn't so any more. We welcome any contribution anyone may bring.

There are many ways you can get involved in KPhotoAlbum. We introduce some ways here, you're welcome to participate in any of these or come up with your own way to help KPhotoAlbum. If you're interested, please also have a look at the [Communication Media](/communication) to get in touch with the current users and developers, we'll be able to help you start with KPhotoAlbum

### Coding

There is so much more we could do if we just had the time. If you want to get involved in coding then read [First Step as a KPhotoAlbum Developer](https://www.kphotoalbum.org/gettinginvolved/coding/). Next step may be to find a junior job - that is a job that will let you get started with KPhotoAlbum without loosing too much hair on the way.

### Bug fixing

This might be a way to get into the code, and also an easy way to help without too much commitment in the beginning. Check the reported bugs at [KDE Bugzilla](https://bugs.kde.org/describecomponents.cgi?product=kphotoalbum) and check the 'Coding' above to find out how to get access to the code.

### Graphics design

KPhotoAlbum is a truly useful application, we have the words of many people, but it might not be the sexiest yet. Help make it sexier.


### Translations

KPhotoAlbum is translated to many languages, but there are still languages to pick from. Check the [KDE translation site](http://www.kde.org/getinvolved/translation/).

### Community Support

New users of KPhotoAlbum occasionally visit the IRC channel or write to the mailing list with questions that any seasoned KPhotoAlbum user ought to be able to handle. This also makes the users feel like home amongst other KPhotoAlbum users and they might eventually start helping others or contribute to the project in other ways.

### Documentation

The documentation needs to be constantly updated to cover the latest features of KPhotoAlbum. This mostly means the help files and [FAQ](https://userbase.kde.org/KPhotoAlbum_FAQ)

### Marketing

BLOG about your KPhotoAlbum usage, add KPhotoAlbum to search site etc etc. Maybe a cool presentation video to show the core features of KPhotoAlbum

### Any Questions?

If you have any questions, please check the [Communication Media](/communication) and don't hesitate to ask us!

