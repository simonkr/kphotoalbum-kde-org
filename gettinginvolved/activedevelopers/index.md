---
layout: page
title: Active Developers
sorted: 7
---

{% for dev in site.data.developers %}
<div class="d-flex">
    <div class="flex-fill mb-2">
        <h2 class="mt-0">{{ dev.name }}</h2>
        <strong>Located:</strong> {{ dev.location }}<br />
        {% if dev.homepage %}
        <strong>Homepage:</strong> <a href="{{dev.homepage}}">{{dev.homepage}}</a><br />
        {% endif %}
        <strong>Area of responsability:</strong> {{ dev.areas }}<br />
        <strong>Since:</strong> {{ dev.since }}<br />
    </div>
    <div>
        <img src="{{ dev.image }}" alt="Photo of {{ dev.name }}" />
    </div>
</div>
{{dev.description | markdownify}}
<hr class="mb-4" />
{% endfor %}
