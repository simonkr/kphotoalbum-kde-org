---
layout: page
title: "KPhotoAlbum Testimonials"
---

# KPhotoAlbum Testimonials

<p class="mt-4 card p-3">KPhotoAlbum is the far and away the best freesource program I've found to manage my collection of images, which is currently over 5000 and growing rapidly. It's not uncommon for me to import over 100 images from a single day's shooting, and in those circumstances it's essential for me to be able to identify the new images and catalogue them quickly. KPhotoAlbum excels in this regard.<br><br>
<a href="mailto:rlk@alum.mit.edu">Robert Krawitz</a></p>

<p class="card p-3">ALL people I've showed KPhotoAlbum loved the concept, the only thing that stopped them are that it does not run on Microsoft Windows. (that's a bad for a good : I converted three people to Linux thanks to KPhotoAlbum)<br><br>
<a href="mailto:-NOSPAM-jmfayard@gmail.com">Jean-Michel FAYARD</a></p>

<p class="card p-3">I'm supposing that 'KPhotoAlbum gives me a woody' won't pass the testimonial-filter. Shame. Because it does. Hmm .. how about 'KPhotoAlbum makes my girlfriend uneasy.'<br><br>
<a href="mailto:jedd-NOSPAM-@progsoc.org">Jedd</a>
</p>

<p class="card p-3">KPhotoAlbum is the best Application to manage my photos! At the moment I have 14'000 images in it, and its still fast when searching. And its far more powerfull than sorting them in folders. Thank you Jesper for this great piece of Software! And its definitly an attractive App to show people the power of Linux!<br><br>
    <strong>Daniel Berger</strong>
</p>

<p class="card p-3">When I acquired my first digital camera, me and my wife started taking photos like crazy. The number of photos kept increasing more than linearly on my hard disk and as time went on it was harder and harder to mantain it (and separate the ones I wanted published from the other ones).<br>
I recently started using a KPhotoAlbum to sort my photos and it is great! Now I am able to publish my first selection of photos, and more are comming!<br><br>
<a href="mailto:pupeno@-NOSPAM-pupeno.com">Pupeno</a>
</p>

<p class="card p-3">Wonderful software, vraiment génial. Since I discovered it, that's the software I show to people I want to impress showing them examples of the quality of GNU/Linux.<br><br>
<a href="mailto:bmathus@NOSPAM-free.fr">Baptiste MATHUS</a></p>

<p class="card p-3">I have two children, and each year my wife and I make a calendar featuring photos of them. It's a lot of work, but it's a labor of love, and it allows us to create a Christmas present for all the relatives in one big batch. (We print about 15 of them each year.)<br><br>
We typically start with about 150 photos that look good on first impression. Then we have to do a lot of classifying in order to balance things. We don't want too many of one child and not the other; we don't want to include one set of grandparents without the other; we don't want all of them to be outside; etc. etc. In past years I've created a spreadsheet to allow filtering of photos (by name) by various criteria.<br><br>
During the past year I came across KPhotoAlbum and said, "I HAVE to try that with this year's calendar." And so it's currently underway. I sat down with my children (ages 6 and 8) and we added attributes to ~120 photos. We learned the software together as we went, and meanwhile I realized we were covering a lot of database ideas. We discovered that a photo can belong to more than class of a given category. We discovered how to add categories, and discussed what categories we wanted to add. My 8-year-old especially came to understand that we were adding information for the purpose of sorting and subsetting later. All in all, I think she came to understand a lot about what databases do, and the fact that we were working with photos certainly kept her attention in a way that (other) data wouldn't.<br><br>
<strong>Thanks for this wonderful program!</strong><br>
<a href="mailto:jim_garrett@-NOSPAM-comcast.net">Jim Garrett</a>
</p>

<p class="card p-3">I just made a month's trip to China by train through Russia and Mongolia and took some 4500 shots so now the archive totals around 42 000 photos. I've been using KPA for almost a year and it's really a great tool. I know there are many others but this is the one I like.<br><br>
It's great to know that there are people who work hard to develope the program and implement new features all the time. Since I started to use it, it's become a lot faster and is now really pleasant to use. Jesper &amp; others, you're doing a great work!<br><br>
<a href="mailto:risto-NOSPAM-@kurppa.fi">Risto H. Kurppa</a>
</p>

<p class="card p-3">We have been using darktable for over year and combined with gimp it has provided a set of professional tools on linux for our photography business.  Until we came across KPhotoAlbum, we had not found an application to really manage the photos satisfactorily.  With a collection of over 100,000 photos and growing, this is hugely important for us.  KPhotoAlbum has taken the task in its stride and we are really happy and highly recommend it.  Thanks to all of those involved <br><br>
<a href="http://www.kitamura-studios.com">Kitamura Studios</a></p>
