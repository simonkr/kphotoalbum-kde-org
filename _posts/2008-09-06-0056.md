---
layout: post
title: "KPhotoAlbum Go Cart Team"
date: 2008-09-06 13:09:00
---

<p>
If you only are interested in KPhotoAlbum, and not its developers, stop reading.
</p>

<p>So the week is soon over, we've all been shouting at each other from time to time, but overall I'd say it was reaaaally greeeeat (quote from the movie Office Space, which we saw early on.</p>

<p>
Yesterday evening it was time for a bit of old-style fun (in contrast to the new style fun, called KPhotoAlbum development). We went out driving go cart and bowling. Lets start from the end. Tuomas obviously was a champ in bowling, so he kind of got twice as many point as the rest of us.</p>

<p>Go cart on the other hand was slightly different, the fattest guy on the team was also the fastest (If you think you read fastest twice, then read again :-) - faster by more than TWO rounds. I won't reveal here who won, but I urge you all to read the <a href="http://cia.vc/stats/project/kde/kphotoalbum">commit messages</a> which might sheed some light over it all (e.g. <a href="http://websvn.kde.org/?view=rev&amp;revision=857720">this</a>, <a href="http://websvn.kde.org/?view=rev&amp;revision=857719">this</a> or <a href="http://websvn.kde.org/?view=rev&amp;revision=857759">this</a> message)</p>

<a href="img/gocard.jpg"><img src="img/gocard_small.jpg" alt="Go cart preparation" /></a>

<p>Thursday evening, we btw also did a bit of old style fun: namely flying kites (or as Jesper called it the whole time dragons (which it is called in Danish (hmm to many parentheses (this is not lisp))))

<a href="img/keit.jpg"><img src="img/keit_small.jpg" alt="Kites" /></a>
</p>
<p class="author">-- Jesper </p>
