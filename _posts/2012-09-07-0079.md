---
layout: post
title: "KPhotoAlbum 4.3-beta1"
date: 2012-09-07 08:09:00
---

<p>Whe have a <a
href="http://download.kde.org/unstable/kphotoalbum/4.3-beta1/src/kphotoalbum-4.3-beta1.tar.bz2">beta build</a> available for the upcoming KPhotoAlbum 4.3.
Give it a spin and give feedback to <a
href="mailto:kphotoalbum@mail.kdab.com">the mailing list</a> (IRC and
bug tracking are also viable options).
</p>
<p>
New features concentrate on video support improvements and background
tasks. Also the database backend has been dropped completely leaving the
XML backend as the only option. Automatic stacking of images based on
file version detection has been brought to the maintenance menu giving
control to the user instead of only performing the action upon starting
of KPhotoAlbum.
</p>
<p class="author">— Miika</p>
