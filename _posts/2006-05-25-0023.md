---
layout: post
title: "Google Summer of Code Project on KPhotoAlbum"
date: 2006-05-25 00:05:00
---

<p>Yesterday the google summer of code projects was chosen, and I'm happy
              to announce that Tuomas Suutari (thsuut AT utu DOT fi) was
              elected to work on KPhotoAlbum. Here is his description of
              the project:</p>

<blockquote>
<p>As you maybe already know, currently KPhotoAlbum stores its data in an
                XML file. This file is read into memory on start up, and is
                written to disk when saving.</p>

<p>Jesper suggested on the KPhotoAlbum mailing list that someone would add
                support for SQL database back-end as a Google Summer of
                Code project. He has already made a proof of concept
                implementation for making the back end pluggable.</p>

<p>When I got that mail, I immediately realized that this would be a cool
project for me, because I use KPhotoAlbum regularly and
love programming.</p>
<p>Adding possibility to save image database into SQL database could provide at least following improvements:</p>
<ul>
<li>scale better for large photo collections (uses less memory)</li>
<li>make KPhotoAlbum start and close faster (no loading/saving of the XML file)</li>
<li>add possibility to access the database by multiple users at the same time</li>
</ul>
<p>The last bullet is really the biggest improvement to many users, who might
            want multiple people to tag the set of images at the same time,
            and perhaps allow for adding information from say a web
            interface.</p>
</blockquote>
