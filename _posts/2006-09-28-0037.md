---
layout: post
title: "The votes have been counted, and KPhotoAlbum now has a new splash screen"
date: 2006-09-28 13:09:00
---

<p>The votes have been counted, and KPhotoAlbum now has a new splash
screen. Congratulation to the winner Jaroslav Hola&#328;.</p>
<img alt="Splash Screen" src="http://www.kphotoalbum.org/img/splash.png"/>
