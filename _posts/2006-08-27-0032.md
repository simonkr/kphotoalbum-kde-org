---
layout: post
title: "New Snapshot with DND support for reordering sub categories"
date: 2006-08-27 21:08:00
---

<p>I hate people questioning the usability of KPhotoAlbum. But that is
nothing compared to how much I hate when they are right :-)</p>

<p>This weekend I've been working on implementing sub category reordering
using drag and drop in the annotation dialog. So if you want to tell
KPhotoAlbum that Las Vegas is in USA, just grab it with your mouse, and
drop it on USA.</p>

<p>As a side effect of this, the items in the list boxes have now changed
to check boxes, and an item is marked as on an image when checked (in
contrast to, when selected), I hope this will make KPhotoAlbum just a
little bit more accessible for newcomers.</p>
