---
layout: post
title: "New snapshot - KPA is soon freezing up"
date: 2006-11-15 00:11:00
---

<p>Yet another weekend working hard on KPhotoAlbum to get us closer to a
release. Please everyone hold back anything but important bugs, so I can
get the next version out of the door soon, plleeeeeeaaaase.</p>

<p>This snapshot is only bugfixes compared to the last one, except one new
feature that Christoph Moseler managed to talk me into including. This
features adds the possibilities to see categories in the thumbnail
viewer. Also it is now also possible to assign tokens in the category viewer.</p>
