---
layout: post
title: "New website for KPhotoAlbum"
date: 2004-10-17 00:10:00
---

<p>KPhotoAlbum's website has now been upgraded, so it is much nicer, and hopefully
easier to navigate. Thanks to Marc Cramdal (bonolebonobo-ifrance-com)<br />
I'm still searching for someone to run the
daily maintenance, so if that is a job for you, please contact
<a href="mailto:blackie@kde.org">Jesper</a>.</p>
