---
layout: post
title: "KPhotoAlbum 2.2 is released"
date: 2006-05-09 00:05:00
---

<p>Finally I got KPhotoAlbum 2.2 out of the door (actually it is already a
couple a weeks ago now - yeah I've been busy). The new version contains two
major improvements: (1) Much improved support for EXIF,
including capability to search your images based on EXIF
info. (2) A much improved thumbnail viewer which allows you
to resize the thumbnails to any size simply by dragging with
the middle mouse button.</p>
<p>The following is a list of features added since 2.1:</p>
<ul>
<li>Added a checkbox to the annotation dialog offering to remove annotations. Thanks for Robert L Krawitz for
	patch with this feature.</li>
<li> The datebar now has an explicit button for canceling selection.</li>
<li> Drasticly improved performance of deleting images. Thanks for Robert L Krawitz for his analysis of the problem.</li>
<li> Its now possible to select whether the newest or the oldest thumbnail should be shown first.</li>
<li> Completely reimplemented the thumbnail view, to solve a huge	amount of issues with QIconView.</li>
<li> Added an EXIF dialog (available from the browser), plus the	possibilities to see EXIF tags in the viewer. Set of EXIF tokens	shown is configurable in the settings dialog.</li>
<li> Searches using the description field are now case insensitive.</li>
<li> Added a Exif dialog to the context menu in the browser.</li>
<li> KPhotoAlbum now stores its thumbnails in ~/.thumbnails complying	with many other applications.</li>
<li> Added an option to save the index.xml file in a compressed	format, this speeds up loading the xml file by approximate a	factor 2.</li>
<li> KPhotoAlbum does now offer to save numbered backups of the index.xml	file: index.xml~0001~, index.xml~0002~ etc. In addition this file	can be zipped to preserve disk space.</li>
</ul>
<p>The following is a list of important bug fixes since 2.1:</p>
<ul>
<li> Inaccurate times now survives a visit to the annotation dialog.</li>
<li> Member maps wasn't correctly renamed when renaming a category.</li>
<li> Fixed crash when annotation dialog was up, and the	datebar got a mouse move event. Thanks to Martin Hoeller for finding this crash.</li>
<li> Fixed bug where limited images on the date bar followed by a	limiting images from the browser, and then unlimiting from the	browser resulted in only items in the scope of the datebar	selection being shown on the datebar.</li>
<li> Restart slideshow timer when user manually moves to a	new image.</li>
</ul>
<p>Finally, since the previous KPhotoAlbum release (back in the days when
it was called KimDaBa), I've worked very hard on trying to
add an SQL backend. This required rather a lot of
restructuring of KPhotoAlbum. Unfortunately I had to give up
on the task before it was finished, as it simply ate me up
from the inside. The good news is that all the changes is
still in there, and it seems like we might get a Google
Summer of Code project for making the darn thing work.</p>
