---
layout: post
title: "KPhotoAlbum 4.0.1 released"
date: 2009-05-21 19:05:00
---

<p>
A new bugfix release have been made of KPhotoAlbum. Changes since the 4.0
release are:</p>
<ul>
<li>The handbook are now included in the package</li>
<li>Fix compilation with new Marble and armel (Marble changed API again)</li>
<li> fixed bug #186807 - no cancel button in "create own database dialog</li>
<li> fixed bug #192385 - it was not possible to hide items in the annotation between sessions.</li>
<li> Changed default shortcut for copy from previous image in the annotation dialog to Alt+insert, as control+insert was eaten by
	the line edits, and the shortcut did thus not work when they had
	the keyboard focus.</li>
<li> BUGFIX: The HTML generator did not display the available themes
	correctly. Thanks to theBro@luukku.com for a patch for this.</li>
<li> BUGFIX: Invoke external application for multiple files did not
	work. Thanks to theBro@luukku.com for a patch for this.</li>

</ul>
