---
layout: page
title: Documentation
css-include: /css/main.css
sorted: 1
---

# Documentation

There are many great features. Many are really straightforward.

The good starting point is to look at [this fast overview](/documentation/3minutetour) and to watch these videos. A [PDF version](http://docs.kde.org/development/en/extragear-graphics/kphotoalbum/kphotoalbum.pdf) of the KDE style documentation is available as well as an [on-line version](http://docs.kde.org/development/en/extragear-graphics/kphotoalbum/index.html). Once you have KPhotoAlbum running you should be able to access the documentation under the Help menu using KDE Help Center.

The documentation can always be improved. If you wish to help us don't hesitate to contact us via [IRC](https://riot.im/app/#/room/#kde-welcome:matrix.org) or via our [mailing list](https://kde.org/support/mailinglists/).

## More Information

{% for documentation in site.documentation %}

+ [{{ documentation.title }}]({{ documentation.url }})
{% endfor %}
