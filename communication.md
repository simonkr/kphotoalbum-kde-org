---
layout: page
title: Communication
sorted: 6
---

# Communication Channels

Communication in the KPhotoAlbum community is happening over the following media:

+ [IRC](https://riot.im/app/#/room/#kde-welcome:matrix.org)

+ [Mailling List](http://mail.kdab.com/mailman/listinfo/kphotoalbum/)

+ [Bugzilla](https://bugs.kde.org/enter_bug.cgi?product=kphotoalbum)

+ [Wiki](http://userbase.kde.org/KPhotoAlbum)


## IRC

If you need help with KPhotoAlbum there is an #kdephotoalbum IRC channel, so you can share your ideas or problems with us. With matrix you can also access the Freenode service. See this [tutorial](https://community.kde.org/Matrix#How_do_I_join_an_IRC_channel.3F) to learn more about it.


## Mailling List

Our mailing list can be found at [mail.kdab.com](https://mail.kdab.com/mailman/listinfo/kphotoalbum/). It's used for all kind of communication, discussion, questions and so on.

The discussion lists are archived so it might be useful to read what has happened in the past.

Thanks to [Klarälvdalens Datakonsult AB](http://www.klaralvdalens-datakonsult.se/) for hosting the list!

## Bugzilla - The KDE Bug tracker

Bugs, inconveniences, and feature wishes are all entered into bugzilla.

To enter a bug in bugzilla, you may either go through the [wizard](https://bugs.kde.org/) or go directly to the [KPhotoAlbum Bug Entry](https://bugs.kde.org/enter_bug.cgi?product=kphotoalbum). The later requires that you have an account but following the URL allows you to create an account right away.

Follow this link to see the [list of open issues](https://bugs.kde.org/buglist.cgi?quicksearch=kphotoalbum)

## Wiki

There is also a [wiki](https://userbase.kde.org/KPhotoAlbum) page, which describes various aspects regards the KPhotoAlbum
